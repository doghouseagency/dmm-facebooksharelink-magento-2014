# Doghouse Facebook Share Link

Just a couple of helper classes to make generating Facebook Share urls easier.

## Installation

Install using Composer (preferred) or Modman.

## Usage

Use the product helper like so:

    <a href="<?php echo Mage::helper('facebooksharelink/product')->init($_product) ?>">Share!</a>

Use the generic helper like so:

    Mage::helper('facebooksharelink')->generateUrl($shareurl, $redirecturl, $parms)

If you decide to specify the `$parms`, it has to be an array. See the following link for possible values: https://developers.facebook.com/docs/reference/dialogs/feed/#params

The shareurl and redirecturl defaults to the current url if you omit it.

## About

Initially created for ZOMP, but based on ENJO's code.