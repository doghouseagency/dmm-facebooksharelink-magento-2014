<?php

class Doghouse_FacebookShareLink_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_PATH_ENABLED = 'facebooksharelink/general/enabled';

    const XML_PATH_APP_ID = 'facebooksharelink/general/appid';

    public function isEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLED);
    }

    public function getAppId()
    {
        return Mage::getStoreConfig(self::XML_PATH_APP_ID);
    }

    public function getDefaultShareUrl()
    {
        return Mage::helper('core/url')->getCurrentUrl();
    }

    /**
     * If shareurl, is empty, it uses the current URL
     * This string is HTML escaped and URL encoded
     *
     * For possible parameters, see https://developers.facebook.com/docs/reference/dialogs/feed/#params
     *
     * @param  [String] Share url
     * @param  [Array] $data options array
     * @return [String]       url
     */
    public function generateUrl($shareurl = null, $redirecturl = null, $data = array())
    {

        if(!$shareurl) {
            $shareurl = $this->getDefaultShareUrl();
        }

        if(!$redirecturl) {
            $redirecturl = $shareurl;
        }

        $url = sprintf("https://www.facebook.com/dialog/feed?app_id=%s&amp;link=%s&amp;redirect_uri=%s",
            urlencode($this->getAppId()),
            urlencode($shareurl),
            urlencode($redirecturl)
        );

        if(is_array($data)) {

            foreach($data as $key => $value) {
                $url .= sprintf("&amp;%s=%s",
                    urlencode($key),
                    urlencode($value)
                );
            }

        }

        return $url;
    }
}
