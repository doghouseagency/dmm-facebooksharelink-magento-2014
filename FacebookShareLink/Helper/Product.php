<?php

/**
 * Small helper class to assist in quickly generating a facebook share url for products.
 * Doesn't do anything special really...
 */
class Doghouse_FacebookShareLink_Helper_Product extends Mage_Core_Helper_Abstract
{

    public function init(Mage_Catalog_Model_Product $product)
    {
        $this->_product = $product;
        return $this;
    }

    public function __toString()
    {

        $p = $this->_product;

        $parms = array(
            'name'          => Mage::helper('catalog/output')->productAttribute($p, $p->getName(), 'name'),
            'picture'       => (string)Mage::helper('catalog/image')->init($p, 'image')->resize(500, 500),
            'description'   => Mage::helper('catalog/output')->productAttribute($p, $p->getDescription(), 'description'),
        );

        $shareurl = $p->getProductUrl();

        return Mage::helper('facebooksharelink')->generateUrl($shareurl, null, $parms);

    }

}
